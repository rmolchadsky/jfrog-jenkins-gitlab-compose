
GITLAB:

export GITLAB_HOME=$HOME/gitlab
docker-compose up -d

Configuration:
/Users/radi/gitlab/config/gitlab.rb
gitlab-ctl reconfigure

Monitoring:
gitlab-ctl tail
gitlab-ctl status

gitlab-rake gitlab:check

Check that the app is running with docker-compose ps: